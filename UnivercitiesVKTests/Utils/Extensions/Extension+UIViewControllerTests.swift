//
//  Extension+UIViewControllerTests.swift
//  UnivercitiesVKTests
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import XCTest
@testable import UnivercitiesVK

class Extension_UIViewControllerTests: XCTestCase {
    
    var viewController: UIViewController!
    
    override func setUp() {
        super.setUp()
        viewController = UIViewController()
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    override func tearDown() {
        super.tearDown()
        viewController = nil
    }
    
    func testAlert() {
        viewController.showAlert(alertTitle: "Test Title", message: "Test Message")
        XCTAssertTrue(viewController.presentedViewController is UIAlertController)
    }
}
