//
//  VKManagerTests.swift
//  UnivercitiesVKTests
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import XCTest
@testable import UnivercitiesVK

class VKManagerTests: XCTestCase {
    
    var vkManager: VKManager!

    override func setUp() {
        super.setUp()
        vkManager = VKManager.instance
    }
    
    func testVkSdkInitialized() {
        setUp()
        XCTAssertNotNil(vkManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    

    func testCountriesList() {

        var Success = false
        var Error = false

        let exp = self.expectation(description: "completion")
        vkManager.getCountries() { (result) in
            switch result {
            case .Success(_):
                Success = true
                break
            case .Error(_):
                Error = true
                break
            }
            exp.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)

        if Success {
            XCTAssertTrue(Success)
        } else {
            XCTAssertTrue(Error)
        }
    }
    
    
    func testCitiesList() {
        
        var Success = false
        var Error = false
        
        let exp = self.expectation(description: "completion")
        vkManager.getCities(countryId: 0) { (result) in
            switch result {
            case .Success(_):
                Success = true
                break
            case .Error(_):
                Error = true
                break
            }
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        if Success {
            XCTAssertTrue(Success)
        } else {
            XCTAssertTrue(Error)
        }
    }
    
    
    func testUniversitiesList() {
        
        var Success = false
        var Error = false
        
        let exp = self.expectation(description: "completion")
        vkManager.getUniversities(countryId: 0, cityId: 0) { (result) in
            switch result {
            case .Success(_):
                Success = true
                break
            case .Error(_):
                Error = true
                break
            }
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        if Success {
            XCTAssertTrue(Success)
        } else {
            XCTAssertTrue(Error)
        }
    }
    
    func testUnivercityLocation() {
        
        var Success = false
        var Error = false
        
        let exp = self.expectation(description: "completion")
        vkManager.getUniversityLocation(universityName: "МГУ") { (result) in
            switch result {
            case .Success(_):
                Success = true
                break
            case .Error(_):
                Error = true
                break
            }
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
        
        if Success {
            XCTAssertTrue(Success)
        } else {
            XCTAssertTrue(Error)
        }
    }
}
