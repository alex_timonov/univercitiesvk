//
//  FriendsListViewControllerTests.swift
//  UnivercitiesVKTests
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import XCTest
import CoreLocation

@testable import UnivercitiesVK

class MapViewControllerTests: XCTestCase {
    
    var viewController: MapViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let location = CLLocation(latitude: 0, longitude: 0)
        let universityLocation = UniversityLocation(address: "Test address", location: location)
        XCTAssertNotNil(location)
        XCTAssertNotNil(universityLocation)
        XCTAssertNotNil(universityLocation.address)
        XCTAssertNotNil(universityLocation.location)
        viewController = MapViewController.instance(location: universityLocation)
        _ = viewController.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        viewController = nil
    }
    
    func testIBOutlets() {
        XCTAssertNotNil(viewController.map)
        XCTAssertNotNil(viewController.txtAddress)
        XCTAssertNotNil(viewController.directionsClick)
    }
    
    func testInstance() {
        XCTAssertNotNil(viewController)
    }
    
    func testViewDidLoad() {
        viewController.viewDidLoad()
        XCTAssertNotNil(viewController.viewModel)
        XCTAssertNotNil(viewController.viewModel.getUniversityLocation())
    }
}
