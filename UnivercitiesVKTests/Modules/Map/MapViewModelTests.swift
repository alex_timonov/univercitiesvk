//
//  FriendsListViewModelTests.swift
//  UnivercitiesVKTests
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import XCTest
import CoreLocation
@testable import UnivercitiesVK

class MapViewModelTests: XCTestCase {
    
    var viewModel: MapViewModel!
    
    let address = "Test Address"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewModel = MapViewModel()
        let point = CLLocation(latitude: 56.22, longitude: -45.21)
        viewModel.universityLocation = UniversityLocation(address: address, location: point)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        viewModel = nil
    }
    func testAddress() {
        XCTAssertEqual(address, viewModel.getAddress())
    }
    func testLocation() {
        XCTAssertEqual(viewModel.universityLocation.location.coordinate.latitude, viewModel.getCoordinate().latitude)
        XCTAssertEqual(viewModel.universityLocation.location.coordinate.longitude, viewModel.getCoordinate().longitude)
        XCTAssertNotNil(viewModel.getAnnotation())
    }
}
