//
//  LoginViewModelTests.swift
//  UnivercitiesVKTests
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import XCTest
@testable import UnivercitiesVK

class SearchViewModelTests: XCTestCase {
    
    var viewModel: SearchViewModel!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewModel = SearchViewModel()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        viewModel = nil
    }
    
    func getDataTest() {
        let countriesExp = expectation(description: "Countries")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            countriesExp.fulfill()
        }
        _ = XCTWaiter.wait(for: [countriesExp], timeout: 1.0)
        XCTAssertNotEqual(viewModel.countries.count, 0)
        
        let citiesExp = expectation(description: "Cities")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            citiesExp.fulfill()
        }
        _ = XCTWaiter.wait(for: [citiesExp], timeout: 1.0)
        XCTAssertNotEqual(viewModel.cities.count, 0)
        
        let universitiesExp = expectation(description: "Universities")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            universitiesExp.fulfill()
        }
        _ = XCTWaiter.wait(for: [universitiesExp], timeout: 1.0)
        XCTAssertNotEqual(viewModel.universities.count, 0)
    }
    
    func testAlertMessageDidSet() {
        
        let exp = expectation(description: "fakeClosure")
        
        var closureCalled = false
        let fakeClosure = {
            closureCalled = true
            exp.fulfill()
        }
        
        viewModel.showAlertClosure = fakeClosure
        viewModel.alertMessage = "message"
        
        waitForExpectations(timeout: 1, handler: nil)
        
        XCTAssertTrue(closureCalled)
    }
}
