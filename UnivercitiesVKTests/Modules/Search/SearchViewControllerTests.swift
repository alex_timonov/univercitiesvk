//
//  SearchViewControllerTests.swift
//  UnivercitiesVKTests
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import XCTest
@testable import UnivercitiesVK

class SearchViewControllerTests: XCTestCase {
    
    var viewController: SearchViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewController = Storyboard.main.instantiateViewController(withIdentifier: "searchVC") as? SearchViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
        viewController.loadView()
        viewController.bindViewModel()
        let exp = expectation(description: "Loading data...")
        _ = XCTWaiter.wait(for: [exp], timeout: 3.0)
      
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        viewController = nil
    }
    
    func testSearchButtonTapped() {
        XCTAssertNotEqual(viewController.txtUnivercity.text.count, 0)
        viewController.searchClick(UIButton())
        let exp = expectation(description: "presentedControllerShowed")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            exp.fulfill()
        }
        _ = XCTWaiter.wait(for: [exp], timeout: 1.0)
        XCTAssertNotEqual(viewController,
                          viewController.navigationController?.presentedViewController)
    }
}
