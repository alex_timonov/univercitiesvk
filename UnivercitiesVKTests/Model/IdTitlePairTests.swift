//
//  FriendTests.swift
//  UnivercitiesVKTests
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import UnivercitiesVK

class IdTitlePairTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMap() {
        
        struct FakeIdTitlePairData {
            let id: Int!
            let title: String!
        }
        
        let fakeData = FakeIdTitlePairData(id: 1, title: "Test")
        
        let jsonString = "{\"id\" : \(fakeData.id!),  \"title\" : \"\(fakeData.title!)\" }"
        
        let idTitlePair = IdTitlePair(JSONString: jsonString)
        
        XCTAssertNotNil(idTitlePair)
        XCTAssertEqual(idTitlePair!.id, fakeData.id)
        XCTAssertEqual(idTitlePair!.title, fakeData.title)
    }
}
