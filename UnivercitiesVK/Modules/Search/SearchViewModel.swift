//
//  LoginViewModel.swift
//  UnivercitiesVK
//
//  Created by Alexey Timonov on 24.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import Foundation

@objcMembers class SearchViewModel: NSObject {
    
    static let TAG_PICKER_COUNTRIES = 0;
    static let TAG_PICKER_CITIES = 1;
    static let TAG_PICKER_UNIVERSITIES = 2;
    
    var countries = [IdTitlePair]()
    var cities = [IdTitlePair]()
    var universities = [IdTitlePair]()
    
    private var universityLocation = UniversityLocation()
    
    var successCountries: (()->())?
    var successCities: (()->())?
    var successUniversities: (()->())?
   
    
    var selectedCountryIndex : Int!
    var selectedCityIndex : Int!
    var selectedUniversityIndex : Int!
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    var showAlertClosure: (()->())?
    

    
    func getCountries() {
        VKManager.instance.getCountries { [weak self] (result) in
            switch result {
            case .Success(let countries):
                self?.countries = countries
                self?.selectedCountryIndex = 0
                self?.successCountries?()
                break
            case .Error(let error):
                self?.alertMessage = error.localizedDescription
                break
            }
        }
    }
    func getCities() {
        VKManager.instance.getCities(countryId: countries[selectedCountryIndex].id) { [weak self] (result) in
            switch result {
            case .Success(let cities):
                self?.cities = cities
                self?.selectedCityIndex = 0
                self?.successCities?()
                break
            case .Error(let error):
                self?.alertMessage = error.localizedDescription
                break
            }
        }
    }
    
    func getUniversities() {
        VKManager.instance.getUniversities(countryId: countries[selectedCountryIndex].id, cityId: cities[selectedCityIndex].id) { [weak self] (result) in
            switch result {
            case .Success(let universities):
                self?.universities = universities
                self?.selectedUniversityIndex = 0
                self?.successUniversities?()
                break
            case .Error(let error):
                self?.alertMessage = error.localizedDescription
                break
            }
        }
    }
    
    func getSelectedCountryName() -> String {
        return countries[selectedCountryIndex].title;
    }
    
    func getSelectedCityName() -> String {
        return cities[selectedCityIndex].title;
    }
    
    func getSelectedUniversityName() -> String {
        return universities.count == 0 ? "No universities" : universities[selectedUniversityIndex].title;
    }
    
    func getSelectedUniversityLocation() -> UniversityLocation {
        return universityLocation
    }
    
    func getPickerItemsCount(tag: Int) -> Int {
        switch tag {
        case SearchViewModel.TAG_PICKER_CITIES:
            return cities.count
        case SearchViewModel.TAG_PICKER_UNIVERSITIES :
            return universities.count
        default:
            return countries.count
        }
    }
    
    func getPickerTitleWith(tag: Int, position: Int) -> String {
        switch tag {
        case SearchViewModel.TAG_PICKER_CITIES:
            return cities[position].title
        case SearchViewModel.TAG_PICKER_UNIVERSITIES :
            return universities[position].title
        default:
            return countries[position].title
        }
    }
    
    func setSelection(tag: Int, position: Int) -> Void {
        switch tag {
        case SearchViewModel.TAG_PICKER_CITIES:
            selectedCityIndex = position
        case SearchViewModel.TAG_PICKER_UNIVERSITIES :
            selectedUniversityIndex = position
        default:
            selectedCountryIndex = position
        }
    }
}
