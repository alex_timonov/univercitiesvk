//
//  SearchViewController.h
//  UnivercitiesVK
//
//  Created by Alex Timonov on 12/22/18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//
#import <UIKit/UIKit.h>
@import VK_ios_sdk;

@interface SearchViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
  
@property (weak, nonatomic) IBOutlet UITextView *txtCountry;
@property (weak, nonatomic) IBOutlet UITextView *txtCity;
@property (weak, nonatomic) IBOutlet UITextView *txtUnivercity;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;

- (void) initViewModel;
- (void) bindViewModel;
- (void) showPickerWith: (NSInteger) tag;
  
- (IBAction)searchClick:(id)sender;

@end
