//
//  SearchViewController.m
//  UnivercitiesVK
//
//  Created by Alex Timonov on 12/22/18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

#import "SearchViewController.h"
#import "UnivercitiesVK-Swift.h"

@interface SearchViewController ()
    @property (strong,nonatomic) SearchViewModel *viewModel;
@end

@implementation SearchViewController
    
    - (void)viewDidLoad {
        [super viewDidLoad];
        [self initViewModel];
        [self bindViewModel];
    }
 
    -(void)viewDidAppear:(BOOL)animated {
        [super viewDidAppear:animated];
        [_txtCountry addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(countryClicked)]];
        [_txtCity addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cityClicked)]];
        [_txtUnivercity addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(universityClicked)]];
        _pickerView.delegate  = self;
        _pickerView.dataSource = self;
    }
         
    
    
    -(void) countryClicked {
        [self showPickerWith: SearchViewModel.TAG_PICKER_COUNTRIES];
    }
         
    -(void)cityClicked {
        [self showPickerWith: SearchViewModel.TAG_PICKER_CITIES];
    }
         
   -(void)universityClicked {
       [self showPickerWith: SearchViewModel.TAG_PICKER_UNIVERSITIES];
    }
         
    -(void)searchClick:(id)sender {
        MapViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
        controller.universityName = [self.viewModel getSelectedUniversityName];
        [self.navigationController  pushViewController:controller animated:YES];
    }
    
    - (void) initViewModel {
        __weak SearchViewController *weakSelf = self;
        self.viewModel = [SearchViewModel new];
        self.viewModel.showAlertClosure = ^{
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if(weakSelf.viewModel.alertMessage) {
                    [weakSelf showAlertWithAlertTitle:@"Error" message:weakSelf.viewModel.alertMessage];
                }
            });
        };
        self.viewModel.successCountries = ^{
            weakSelf.txtCountry.text = [weakSelf.viewModel getSelectedCountryName];
            [weakSelf.viewModel getCities];
        };
        self.viewModel.successCities = ^{
            weakSelf.txtCity.text = [weakSelf.viewModel getSelectedCityName];
            [weakSelf.viewModel getUniversities];
        };
        self.viewModel.successUniversities = ^{
            weakSelf.txtUnivercity.text = [weakSelf.viewModel getSelectedUniversityName];
            weakSelf.btnSearch.hidden = NO;
        };
    }
    
    - (void) bindViewModel {
        [self.viewModel getCountries];
    }
  
    - (void) showPickerWith: (NSInteger) tag {
        [_pickerView setHidden:NO];
        _pickerView.tag = tag;
        [_pickerView reloadComponent:0];
    }
    
    - (void) onCountrySelected {
        _txtCountry.text = [self.viewModel getSelectedCountryName];
        [self.viewModel getCities];
    }
    
    - (void) onCitySelected {
       _txtCity.text = [self.viewModel getSelectedCityName];
        [self.viewModel getUniversities];
    }
    
    - (void) onUniversitySelected {
        _txtUnivercity.text = [self.viewModel getSelectedUniversityName];
    }
    
    -(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
        [self.viewModel setSelectionWithTag:pickerView.tag position:row];
        switch (pickerView.tag) {
            case 1:
              [self onCitySelected];
              break;
            case 2:
            [self onUniversitySelected];
            break;
        
            default:
            [self onCountrySelected];
        }
    }
    -(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
        return [self.viewModel getPickerTitleWithTag:pickerView.tag position:row];
    }
    
    - (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
      return 1;
   }
    
    - (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
        return [self.viewModel getPickerItemsCountWithTag:pickerView.tag];
    }
    
@end
    
    
    
    
