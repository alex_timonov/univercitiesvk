//
//  MapViewController.swift
//  UnivercitiesVK
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var txtAddress: UILabel!

    @IBAction func directionsClick(_ sender: UIButton) {
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: viewModel.getCoordinate()))
        destination.name = viewModel.universityLocation.address
        MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    let viewModel = MapViewModel()
    
    @objc var universityName: String! {
        didSet {
            viewModel.universityName = universityName
        }
    }
    
    class func instance(location: UniversityLocation) -> MapViewController {
        let controller = Storyboard.main.instantiateViewController(withIdentifier: "MapVC") as! MapViewController
        controller.universityName = location.address
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
        viewModel.getUniversityLocation()
    }
    
    
    func initViewModel() {
        viewModel.successUniversityLocation = { [weak self] in
            self?.bindModel()
        }
    }
    
    private func bindModel() {
        txtAddress.text = viewModel.getAddress()
        map.setRegion(MKCoordinateRegion(center: viewModel.getCoordinate(), span: MKCoordinateSpan.init(latitudeDelta: 0.05, longitudeDelta: 0.05)), animated: true)
        map.addAnnotation(viewModel.getAnnotation())
        map.showAnnotations([viewModel.getAnnotation()], animated: true)

    }
}
