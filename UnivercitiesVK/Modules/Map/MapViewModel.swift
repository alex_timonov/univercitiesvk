//
//  UniversityDetailViewModel.swift
//  UnivercitiesVK
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import Foundation
import MapKit

class MapViewModel: NSObject {
    
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    var showAlertClosure: (()->())?
    
    var universityLocation: UniversityLocation!
    var universityName: String!
    
    var successUniversityLocation: (()->())?
    
    func getAnnotation() ->  MKPointAnnotation {
        let annotation = MKPointAnnotation()
        annotation.coordinate  = universityLocation.location.coordinate
        return annotation
    }
    
    func getAddress() ->  String {
        return universityLocation.address
    }
    
    func getCoordinate() ->  CLLocationCoordinate2D {
        return universityLocation.location.coordinate
    }
    
    func getUniversityLocation() {
        VKManager.instance.getUniversityLocation(universityName: universityName) { [weak self] (result) in
            switch result {
            case .Success(let location):
                self?.universityLocation = location
                self?.successUniversityLocation?()
                break
            case .Error(let error):
                self?.alertMessage = error.localizedDescription
                break
            }
        }
    }
}

