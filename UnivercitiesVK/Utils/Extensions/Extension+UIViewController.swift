//
//  Extension+UIViewController.swift
//  UnivercitiesVK
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import Foundation
import UIKit

@objc extension UIViewController {
    
    func showAlert(alertTitle: String, message: String) {
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
