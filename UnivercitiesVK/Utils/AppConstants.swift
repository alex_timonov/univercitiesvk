//
//  AppConstants.swift
//  UnivercitiesVK
//
//  Created by Alexey Timonov on 24.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import UIKit

struct Storyboard {
    static let main = UIStoryboard(name: "Main", bundle: nil)
}
