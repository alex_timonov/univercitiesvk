//
//  VKManager.swift
//  UnivercitiesVK
//
//  Created by Alexey Timonov on 24.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import Foundation
import VK_ios_sdk
import SwiftyJSON
import CoreLocation
import Contacts

private let APP_ID = "6792779"
private let SERVICE_KEY = "346b265c346b265c346b265cb7340c80173346b346b265c6877d59220d032c4cc9ba6ce"

enum Result<T> {
    case Success(T)
    case Error(Error)
}

class VKManager: NSObject {
    
    static let instance = VKManager()
    private override init() {}

    func setup() {
         VKSdk.initialize(withAppId: APP_ID)
    }
    
    func getCountries(completion: @escaping (Result<[IdTitlePair]>) -> Void) {

        let countriesRequest = VKApi.request(withMethod: "database.getCountries", andParameters: ["need_all":"1", "count":"1000","access_token":SERVICE_KEY])

        countriesRequest?.execute(resultBlock: { (response) in
            let json = JSON(arrayLiteral: response!.json)
            if let items = json[0].dictionary!["items"]?.arrayValue {
                let result = items.compactMap({ (item) -> IdTitlePair? in
                    if let idTitlePair = IdTitlePair(JSONString: item.description) {
                        return idTitlePair
                    }
                    return nil
                })
                completion(.Success(result))
            } else {
                completion(.Error(NSError(domain:"", code:0, userInfo:nil)))
            }
        }, errorBlock: { (error) in
            if (error! as NSError).code != VK_API_ERROR {
                (error! as NSError).vkError.request.repeat()
                completion(.Error(error!))
            } else {
                completion(.Error(error!))
            }
        })
    }

    func getCities(countryId: Int, completion: @escaping (Result<[IdTitlePair]>) -> Void) {
        
        let regionsRequest = VKApi.request(withMethod: "database.getCities", andParameters: ["country_id":countryId, "access_token":SERVICE_KEY])
        
        regionsRequest?.execute(resultBlock: { (response) in
            let json = JSON(arrayLiteral: response!.json)
            if let items = json[0].dictionary!["items"]?.arrayValue {
                let result = items.compactMap({ (item) -> IdTitlePair? in
                    if let idTitlePair = IdTitlePair(JSONString: item.description) {
                        return idTitlePair
                    }
                    return nil
                })
                  completion(.Success(result))
            } else {
                 completion(.Error(NSError(domain:"", code:0, userInfo:nil)))
            }
        }, errorBlock: { (error) in
            if (error! as NSError).code != VK_API_ERROR {
                (error! as NSError).vkError.request.repeat()
                completion(.Error(error!))
            } else {
                completion(.Error(error!))
            }
        })
    }
    
    
    func getUniversities(countryId: Int, cityId: Int, completion: @escaping (Result<[IdTitlePair]>) -> Void) {
        
        let universitiesRequest = VKApi.request(withMethod: "database.getUniversities", andParameters: ["country_id":countryId, "city_id":cityId, "access_token":SERVICE_KEY])
        universitiesRequest?.execute(resultBlock: { (response) in
            let json = JSON(arrayLiteral: response!.json)
            if let items = json[0].dictionary!["items"]?.arrayValue {
                let result = items.compactMap({ (item) -> IdTitlePair? in
                    if let idTitlePair = IdTitlePair(JSONString: item.description) {
                        return idTitlePair
                    }
                    return nil
                })
                  completion(.Success(result))
            } else {
                 completion(.Error(NSError(domain:"", code:0, userInfo:nil)))
            }
        }, errorBlock: { (error) in
            if (error! as NSError).code != VK_API_ERROR {
                (error! as NSError).vkError.request.repeat()
                completion(.Error(error!))
            } else {
                completion(.Error(error!))
            }
        })
    }
    
    func getUniversityLocation( universityName : String,
                        completion: @escaping (Result<UniversityLocation>) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(universityName) { (placemarks, error) in
            if error == nil {
                if let placemark = placemarks?[0] {
                    var universityLocation = UniversityLocation()
                    universityLocation.address = CNPostalAddressFormatter.string(from:placemark.postalAddress!, style: .mailingAddress)

                    universityLocation.location = placemark.location!
                    completion(.Success(universityLocation))
                    
                }
            } else {
                completion(.Error(error!))
            }
        }
    }
}
