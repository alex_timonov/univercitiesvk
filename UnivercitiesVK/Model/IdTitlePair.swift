//
//  Friend.swift
//  UnivercitiesVK
//
//  Created by Alexey Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import Foundation
import ObjectMapper

public struct IdTitlePair: Mappable {
    var id: Int!
    var title: String!
    
    public init?(map: Map){ }
    
    mutating public func mapping(map: Map) {
        id          <- map["id"]
        title       <- map["title"]
    }
}

