//
//  UniversityLocation.swift
//  UnivercitiesVK
//
//  Created by Alex Timonov on 21.12.18.
//  Copyright © 2018 Alexey Timonov. All rights reserved.
//

import CoreLocation

public struct UniversityLocation {
    var address: String!
    var location: CLLocation!
}
